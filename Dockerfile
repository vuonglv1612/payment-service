FROM python:3.8.13-slim-buster
WORKDIR /app
COPY requirements.txt /tmp/requirements.txt
RUN pip install -U pip==20.0.2 && pip install -r /tmp/requirements.txt
COPY . .
CMD [ "python", "api.py" ]
