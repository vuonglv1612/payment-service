import uvicorn

from config import create_settings
from payment_service.api.app import app

if __name__ == "__main__":
    settings = create_settings()
    uvicorn.run(app, host=settings.api.host, port=settings.api.port)
