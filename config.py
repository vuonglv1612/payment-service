from functools import lru_cache
import os
from pydantic import BaseSettings


class APISettings(BaseSettings):
    """API settings."""

    host: str = "0.0.0.0"
    port: int = 8080

    class Config:
        env_prefix = "API_"


class VNPaySettings(BaseSettings):
    """VNPay settings."""

    merchant_code: str
    secret_key: str
    pay_url: str
    query_url: str
    return_url: str

    class Config:
        env_prefix = "VNPAY_"


class PostgresSettings(BaseSettings):
    uri: str
    pool_size: int = 20
    max_overflow: int = 10
    pool_recycle: int = 3000

    class Config:
        env_prefix = "POSTGRES_"


class AdminTokenSettings(BaseSettings):
    base_url: str
    username: str
    password: str
    client_id: str
    client_secret: str

    class Config:
        env_prefix = "ADMIN_TOKEN_"


class CachingSettings(BaseSettings):
    redis_uri: str

    class Config:
        env_prefix = "CACHING_"


class AccountServiceSettings(BaseSettings):
    base_url: str

    class Config:
        env_prefix = "ACCOUNT_SERVICE_"


class Settings:
    debug = os.getenv("DEBUG", "0") == "1"
    if debug:
        from dotenv import load_dotenv

        load_dotenv()

    api = APISettings()
    vnpay = VNPaySettings()  # type: ignore
    postgres = PostgresSettings()  # type: ignore
    admin_token = AdminTokenSettings()  # type: ignore
    caching = CachingSettings()  # type: ignore
    account_service = AccountServiceSettings()  # type: ignore


@lru_cache
def create_settings():
    return Settings()
