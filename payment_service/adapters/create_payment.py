from sqlalchemy.orm import Session

from payment_service.core.create_payment import ports
from payment_service.libs.postgres import models
from payment_service.libs.vnpay import VNPay


class PostgresPaymentRepository(ports.PaymentRepository):
    def __init__(self, session: Session):
        self.session = session

    def save(self, payment: ports.Payment) -> None:
        orm_payment = models.Payment(
            id=payment.id,
            created_at=payment.created_at,
            updated_at=payment.updated_at,
            account_id=payment.account_id,
            payment_method=payment.payment_method,
            amount=payment.amount,
            status=payment.status,
        )
        self.session.add(orm_payment)
        self.session.commit()


class VNPayBankRepository(ports.BankRepository):
    def __init__(self, vnpay_client: VNPay):
        self.vnpay_client = vnpay_client

    def create_payment_url(self, client_ip: str, payment: ports.Payment) -> str:
        url = self.vnpay_client.get_payment_url(
            order_id=payment.id, amount=payment.amount, created_at=payment.created_at, client_ip=client_ip
        )
        return url
