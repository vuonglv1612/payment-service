from typing import Union
from sqlalchemy.orm import Session

from payment_service.core.finish_payment import errors, ports
from payment_service.libs.admin_token import AdminToken
from payment_service.libs.postgres.models import Payment
import requests


class PostgresFinishPaymentRepository(ports.FinishPaymentRepository):
    def __init__(self, session: Session):
        self.session = session

    def get_payment(self, payment_id: str) -> Union[Payment, None]:
        payment_in_db = self.session.query(Payment).filter(Payment.id == payment_id).first()
        if not payment_in_db:
            return None
        payment = ports.Payment(
            id=payment_in_db.id,
            created_at=payment_in_db.created_at,
            updated_at=payment_in_db.updated_at,
            account_id=payment_in_db.account_id,
            payment_method=payment_in_db.payment_method,
            amount=payment_in_db.amount,
            status=payment_in_db.status,
        )
        return payment

    def finish_payment(self, payment_id: str, status: str) -> ports.Payment:
        payment_in_db = self.session.query(Payment).filter(Payment.id == payment_id).first()
        if not payment_in_db:
            raise errors.PaymentNotFound(payment_id=payment_id)
        payment_in_db.status = status
        self.session.commit()
        payment = ports.Payment(
            id=payment_in_db.id,
            created_at=payment_in_db.created_at,
            updated_at=payment_in_db.updated_at,
            account_id=payment_in_db.account_id,
            payment_method=payment_in_db.payment_method,
            amount=payment_in_db.amount,
            status=payment_in_db.status,
        )
        return payment


class AccountBalanceRepository(ports.BalanceRepository):
    def __init__(self, base_url: str, token: AdminToken):
        self.base_url = base_url
        self.token = token

    def increase(self, payment: ports.Payment) -> ports.Balance:
        endpoint = "/api/v1/account/balance_adjustment"
        url = f"{self.base_url}{endpoint}"
        token = self.token.get_token()
        header = {"Authorization": f"Bearer {token}"}
        message = "TOPUP - Payment: {}".format(payment.id)
        data = {
            "accountId": payment.account_id,
            "amount": payment.amount,
            "message": message,
            "paymentId": payment.id,
            "paymentMethod": payment.payment_method,
        }
        response = requests.post(url, headers=header, json=data)
        if response.status_code != 200:
            raise errors.IncreaseBalanceError(
                payment_id=payment.id,
                reason=f"{response.status_code} - {response.text}",
            )
        status = response.json()["code"]
        if status != 200:
            raise errors.IncreaseBalanceError(
                payment_id=payment.id,
                reason=f"{status} - {response.json()['message']}",
            )
        data = response.json().get("data", {})
        return ports.Balance(
            account_id=data.get("accountId"),
            amount=data.get("amount"),
        )
