import os
import uuid

from fastapi import FastAPI, Request
from fastapi.responses import JSONResponse, RedirectResponse
from loguru import logger

from .create_payment.route import router as create_payment_router
from .vnpay_callback.route import router as vnpay_callback_router

app = FastAPI(title="Payment Service", description="Payment Service")


@app.exception_handler(Exception)
async def handle_exception(request: Request, exc: Exception):
    request_id = request.scope.get("request_id")
    logger.error("Request [{}] - Error: {}", request_id, exc)
    return JSONResponse(
        status_code=500,
        content={"detail": {"message": "Internal server error", "request_id": request.scope.get("request_id")}},
    )


@app.get("/", include_in_schema=False)
def redirect_to_doc():
    return RedirectResponse(url="/docs")


@app.middleware("http")
async def add_request_id(request: Request, call_next):
    request_id = str(uuid.uuid4())
    logger.info(
        "Request [{}] - {} {}",
        request_id,
        request.method,
        request.url,
    )
    request.scope["request_id"] = request_id
    response = await call_next(request)
    logger.info(
        "Request [{}] - {} {}",
        request_id,
        response.status_code,
        response.headers,
    )
    return response


if os.getenv("API_PRIVATE", "1") == "1":
    app.include_router(create_payment_router, prefix="/payments", tags=["payment"])
app.include_router(vnpay_callback_router, prefix="/callback", tags=["callbacks"])
