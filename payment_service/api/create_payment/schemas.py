from datetime import datetime
from typing import Optional
from enum import Enum

from pydantic import BaseModel, Field, validator


class PaymentMethod(str, Enum):
    vnpay = "vnpay"


class Payment(BaseModel):
    id: str
    account_id: str
    payment_method: PaymentMethod
    amount: float = Field(gt=0)
    status: str
    created_at: Optional[datetime] = None
    updated_at: Optional[datetime] = None


class CreatePaymentBodySchema(BaseModel):
    payment_method: PaymentMethod
    account_id: str
    amount: float

    @validator("amount")
    def validate_amount(cls, v):
        if v <= 0:
            raise ValueError("Số tiền phải lớn hơn 0")
        return v


class CreatePaymentResponseSchema(BaseModel):
    payment: Payment
    timestamp: int
    payment_url: str
    client_ip: Optional[str] = None
