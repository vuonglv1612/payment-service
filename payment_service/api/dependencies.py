from fastapi import Depends

from config import Settings, create_settings
from payment_service.libs.postgres.connection import PostgresConnection


def postgres_connection(settings: Settings = Depends(create_settings)):
    return PostgresConnection(
        db_uri=settings.postgres.uri,
        pool_size=settings.postgres.pool_size,
        pool_max_overflow=settings.postgres.max_overflow,
        pool_recycle=settings.postgres.pool_recycle,
    )
