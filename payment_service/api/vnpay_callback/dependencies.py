from fastapi import Depends

from config import Settings, create_settings
from payment_service.adapters.finish_payment import AccountBalanceRepository, PostgresFinishPaymentRepository
from payment_service.core.finish_payment import ports
from payment_service.core.finish_payment.services import FinishPaymentService
from payment_service.libs.admin_token import AdminToken
from payment_service.libs.vnpay import VNPay

from ..dependencies import postgres_connection


def vnpay_client(settings: Settings = Depends(create_settings)) -> VNPay:
    return VNPay(
        settings.vnpay.merchant_code,
        settings.vnpay.secret_key,
        settings.vnpay.pay_url,
        settings.vnpay.query_url,
        settings.vnpay.return_url,
    )


def create_admin_token(settings: Settings = Depends(create_settings)) -> AdminToken:
    return AdminToken(
        settings.caching.redis_uri,
        settings.admin_token.base_url,
        settings.admin_token.username,
        settings.admin_token.password,
        settings.admin_token.client_id,
        settings.admin_token.client_secret,
    )


def finish_payment_repo(connection=Depends(postgres_connection)) -> ports.FinishPaymentRepository:
    session = connection()
    return PostgresFinishPaymentRepository(session)


def finish_payment_balance_repo(
    settings=Depends(create_settings), admin_token=Depends(create_admin_token)
) -> ports.BalanceRepository:
    return AccountBalanceRepository(settings.account_service.base_url, admin_token)


def finish_payment_service(
    finish_payment_repo: ports.FinishPaymentRepository = Depends(finish_payment_repo),
    balance_repo: ports.BalanceRepository = Depends(finish_payment_balance_repo),
) -> ports.FinishPaymentRepository:
    return FinishPaymentService(finish_payment_repo, balance_repo)
