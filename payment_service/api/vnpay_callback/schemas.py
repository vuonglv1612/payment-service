from typing import Optional
from pydantic import BaseModel


class VNPayCallbackAPIParams(BaseModel):
    vnp_Amount: float
    vnp_BankCode: str
    vnp_CardType: str


class VNPayCallbackAPIResponse(BaseModel):
    payment_id: str
    payment_amount: float
    payment_status: str
    balance_amount: Optional[float] = None
