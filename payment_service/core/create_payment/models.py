from datetime import datetime

from pydantic import BaseModel
from enum import Enum


class PaymentStatus(str, Enum):
    pending = "pending"


class Payment(BaseModel):
    id: str
    created_at: datetime
    updated_at: datetime
    account_id: str
    payment_method: str
    amount: float
    status: PaymentStatus


class CreatePaymentRequest(BaseModel):
    client_ip: str
    payment_method: str
    amount: float
    account_id: str


class CreatePaymentResponse(BaseModel):
    payment: Payment
    timestamp: int
    payment_url: str
