import uuid

from payment_service.libs.datetime import get_current_time, naive_to_aware

from .ports import (
    BankRepository,
    CreatePaymentRequest,
    CreatePaymentResponse,
    CreatePaymentUseCase,
    Payment,
    PaymentRepository,
)


class CreatePaymentService(CreatePaymentUseCase):
    def __init__(self, payment_repository: PaymentRepository, bank_repository: BankRepository):
        self.payment_repository = payment_repository
        self.bank_repository = bank_repository

    def _generate_payment_id(self) -> str:
        return str(uuid.uuid4().hex)

    def create(self, request: CreatePaymentRequest) -> CreatePaymentResponse:
        payment_id = self._generate_payment_id()
        created_at = naive_to_aware(get_current_time())
        payment = Payment(
            id=payment_id,
            created_at=created_at,
            updated_at=created_at,
            account_id=request.account_id,
            payment_method=request.payment_method,
            amount=request.amount,
            status="pending",
        )
        self.payment_repository.save(payment)
        payment_url = self.bank_repository.create_payment_url(client_ip=request.client_ip, payment=payment)
        return CreatePaymentResponse(
            payment=payment,
            timestamp=int(created_at.timestamp()),
            payment_url=payment_url,
        )
