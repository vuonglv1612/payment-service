from datetime import datetime
from enum import Enum
from typing import Optional
from pydantic import BaseModel


class PaymentStatus(str, Enum):
    pending = "pending"
    success = "success"
    failed = "failed"


class Payment(BaseModel):
    id: str
    created_at: datetime
    updated_at: datetime
    account_id: str
    payment_method: str
    amount: float
    status: PaymentStatus


class Balance(BaseModel):
    account_id: str
    amount: float


class FinishPaymentRequest(BaseModel):
    payment_id: str
    status: PaymentStatus


class FinishPaymentResponse(BaseModel):
    payment: Payment
    balance: Optional[Balance] = None
