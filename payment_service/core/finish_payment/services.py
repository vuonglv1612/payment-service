from payment_service.core.finish_payment import errors
from payment_service.core.finish_payment.models import PaymentStatus
from .ports import (
    BalanceRepository,
    FinishPaymentRepository,
    FinishPaymentRequest,
    FinishPaymentResponse,
    FinishPaymentUseCase,
)


class FinishPaymentService(FinishPaymentUseCase):
    def __init__(self, repository: FinishPaymentRepository, balance_repository: BalanceRepository):
        self.repository = repository
        self.balance_repository = balance_repository

    def handle(self, request: FinishPaymentRequest) -> FinishPaymentResponse:
        current_payment = self.repository.get_payment(request.payment_id)
        if not current_payment:
            raise errors.PaymentNotFound(payment_id=request.payment_id)
        if current_payment.status in [PaymentStatus.success, PaymentStatus.failed]:
            raise errors.PaymentAlreadyFinishedError(payment_id=request.payment_id)
        payment = self.repository.finish_payment(request.payment_id, request.status)
        balance = None
        if request.status == PaymentStatus.success:
            balance = self.balance_repository.increase(payment)
        return FinishPaymentResponse(payment=payment, balance=balance)
