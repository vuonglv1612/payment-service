import hashlib

import redis
import requests
import retry


def caching(redis: redis.Redis, key: str, use_cache: bool = True):
    def decorator(func):
        def wrapper(*args, **kwargs):
            if not use_cache:
                return func(*args, **kwargs)
            value = redis.get(key)
            if value is None:
                value = func(*args, **kwargs)
                redis.set(key, value)
            else:
                value = value.decode("utf-8")
            return value

        return wrapper

    return decorator


class AdminToken:
    def __init__(self, redis_uri: str, base_url: str, username: str, password: str, client_id: str, client_secret: str):
        self.base_url = base_url
        self.username = username
        self.password = password
        self.client_id = client_id
        self.client_secret = client_secret
        self.redis = redis.from_url(redis_uri)

    def get_token(self, use_cache: bool = True) -> str:
        token_key = hashlib.sha256(f"{self.username}:{self.password}".encode()).hexdigest()

        @retry.retry(tries=3, delay=1, backoff=2)
        @caching(self.redis, token_key, use_cache)
        def _call_auth_api():
            url = f"{self.base_url}/oauth/token"
            payload = {
                "grant_type": "password",
                "username": self.username,
                "password": self.password,
                "client_id": self.client_id,
                "client_secret": self.client_secret,
            }
            response = requests.post(url, data=payload)
            # TODO: check error
            token = response.json()["access_token"]
            return token

        return _call_auth_api()
