from datetime import datetime
import pytz


def naive_to_aware(naive_datetime: datetime) -> datetime:
    """
    Convert naive datetime to aware datetime
    """
    if naive_datetime.tzinfo is not None:
        return naive_datetime
    return pytz.utc.localize(naive_datetime)


def get_current_time() -> datetime:
    """
    Get current time
    """
    return datetime.now(pytz.utc)
