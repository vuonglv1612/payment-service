from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


def create_connection_factory(db_uri: str, pool_size: int = 20, pool_max_overflow: int = 10, pool_recycle: int = 3000):
    engine = create_engine(db_uri, pool_size=pool_size, max_overflow=pool_max_overflow, pool_recycle=pool_recycle)
    return sessionmaker(bind=engine)


class PostgresConnection:
    _instance = None

    def __new__(cls, db_uri: str, pool_size: int = 20, pool_max_overflow: int = 10, pool_recycle: int = 3000, *_, **__):
        if cls._instance is None:
            cls._instance = create_connection_factory(db_uri, pool_size, pool_max_overflow, pool_recycle)
        return cls._instance
