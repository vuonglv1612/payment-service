from sqlalchemy import Column, String, DateTime, Float
from sqlalchemy.orm import declarative_base
from datetime import datetime, timezone


Base = declarative_base()


def create_default_now():
    return datetime.now(tz=timezone.utc)


class Payment(Base):
    __tablename__ = "payments"

    id = Column(String(36), primary_key=True)  # UUID
    created_at = Column(DateTime, nullable=False, default=create_default_now)
    updated_at = Column(DateTime, nullable=False, default=create_default_now, onupdate=create_default_now)
    account_id = Column(String(36), nullable=False)
    payment_method = Column(String(36), nullable=False)
    amount = Column(Float, nullable=False, default=0)
    status = Column(String, nullable=False)
