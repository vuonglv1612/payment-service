import hashlib
import hmac
import urllib.parse
from datetime import datetime
import requests

import pytz


class VNPay:
    def __init__(self, merchant_code: str, secret_key: str, pay_url: str, query_url: str, return_url: str):
        self.merchant_code = merchant_code
        self.secret_key = secret_key
        self.pay_url = pay_url
        self.query_url = query_url
        self.return_url = return_url

    def _create_query_string(self, params):
        inputData = sorted(params.items())
        query_string = ""
        seq = 0
        for key, val in inputData:
            if seq == 1:
                query_string = query_string + "&" + key + "=" + urllib.parse.quote_plus(str(val))
            else:
                seq = 1
                query_string = key + "=" + urllib.parse.quote_plus(str(val))

        return query_string

    def _normalize_params(self, params):
        if "vnp_Version" not in params:
            params["vnp_Version"] = "2.1.0"
        if "vnp_TmnCode" not in params:
            params["vnp_TmnCode"] = self.merchant_code

    def _create_secure_hash(self, query_string):
        hash_value = self._hash(self.secret_key, query_string)
        return hash_value

    def get_payment_url(
        self,
        order_id: str,
        amount: float,
        created_at: datetime,
        client_ip: str,
        order_info: str = None,
    ):
        amount = int(amount * 100)
        if not order_info:
            order_info = "Thanh toan don hang {}".format(order_id)
        params = {
            "vnp_Amount": amount,
            "vnp_CurrCode": "VND",
            "vnp_Locale": "vn",
            "vnp_CreateDate": created_at.strftime("%Y%m%d%H%M%S"),
            "vnp_OrderInfo": order_info,
            "vnp_OrderType": "topup",
            "vnp_TxnRef": order_id,
            "vnp_IpAddr": client_ip,
        }

        self._normalize_params(params)
        if "vnp_Command" not in params:
            params["vnp_Command"] = "pay"
        if "vnp_ReturnUrl" not in params:
            params["vnp_ReturnUrl"] = self.return_url
        query_string = self._create_query_string(params)
        hash_value = self._create_secure_hash(query_string)
        return self.pay_url + "?" + query_string + "&vnp_SecureHash=" + hash_value

    def validate_response(self, params, input_hash):
        def _create_data(params):
            inputData = sorted(params.items())
            query_string = ""
            seq = 0
            for key, val in inputData:
                if seq == 1:
                    query_string = query_string + "&" + key + "=" + str(val)
                else:
                    seq = 1
                    query_string = key + "=" + str(val)

            return query_string

        data = {**params}
        if "vnp_SecureHash" in params.keys():
            data.pop("vnp_SecureHash")

        if "vnp_SecureHashType" in params.keys():
            data.pop("vnp_SecureHashType")

        query_string = self._create_query_string(data)
        hash_value = self._create_secure_hash(query_string)
        return input_hash == hash_value

    def check_transaction_status(
        self, order_id: str, transaction_no: str, transaction_date: str, client_ip: str = "localhost"
    ):
        params = {
            "vnp_Version": "2.1.0",
            "vnp_Command": "querydr",
            "vnp_TmnCode": self.merchant_code,
            "vnp_TxnRef": order_id,
            "vnp_OrderInfo": "Thanh toan don hang {}".format(order_id),
            "vnp_TransactionNo": transaction_no,
            "vnp_TransDate": transaction_date,
            "vnp_CreateDate": datetime.now(tz=pytz.timezone("Asia/Ho_Chi_Minh")).strftime("%Y%m%d%H%M%S"),
            "vnp_IpAddr": client_ip,
            "vnp_SecureHashType": "HMACSHA512",
        }
        query_string = self._create_query_string(params)
        hash_value = self._create_secure_hash(query_string)
        params["vnp_SecureHash"] = hash_value

        response = requests.post(self.query_url, data=params)
        return response.text

    @staticmethod
    def _hash(key, data):
        byteKey = key.encode("utf-8")
        byteData = data.encode("utf-8")
        return hmac.new(byteKey, byteData, hashlib.sha512).hexdigest()
